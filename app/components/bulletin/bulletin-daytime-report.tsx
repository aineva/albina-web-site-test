import React from "react";
import { FormattedMessage, useIntl } from "../../i18n";
import TendencyIcon from "../icons/tendency-icon";
import BulletinDangerRating from "./bulletin-danger-rating.jsx";
import BulletinProblemItem from "./bulletin-problem-item.jsx";
import BulletinAWMapStatic from "./bulletin-awmap-static.jsx";
import {
  dateToISODateString,
  getSuccDate,
  LONG_DATE_FORMAT
} from "../../util/date.js";
import { Tooltip } from "../tooltips/tooltip";
import {
  matchesValidTimePeriod,
  type Bulletin,
  type Tendency,
  type ValidTimePeriod
} from "../../stores/bulletin";
import { scrollIntoView } from "../../util/scrollIntoView";

type Props = {
  validTimePeriod: ValidTimePeriod;
  bulletin: Bulletin;
  date: Date;
};

function BulletinDaytimeReport({ validTimePeriod, bulletin, date }: Props) {
  const intl = useIntl();
  const problems =
    bulletin?.avalancheProblems?.filter(p =>
      matchesValidTimePeriod(validTimePeriod, p.validTimePeriod)
    ) || [];
  const dangerRatings =
    bulletin?.dangerRatings?.filter(p =>
      matchesValidTimePeriod(validTimePeriod, p.validTimePeriod)
    ) || [];

  const regionNames = [
    ...new Set(
        bulletin?.regions
            ?.map?.(r => r?.regionID) // NOTE: r.name is still a code
            ?.filter?.(id => !!id)
            ?.map?.(id =>
            intl.formatMessage({
                id: "region:" + id
            })
        )
            ?.filter?.(msg => !!msg)
            ?.map?.(msg =>
            msg.startsWith("region:") ? msg.slice("region:".length) : msg
        ) ?? []
    )
  ];


  return (
    <div>
      {validTimePeriod && (
        <h2 className="subheader">
          <FormattedMessage id={`bulletin:report:daytime:${validTimePeriod}`} />
        </h2>
      )}
      <div className="bulletin-report-pictobar">
        <div className="bulletin-report-region">
          <Tooltip
            label={intl.formatMessage({
              id: "bulletin:report:selected-region:hover"
            })}
          >
            <a
              href="#page-main"
              onClick={e => scrollIntoView(e)}
              className="img icon-arrow-up"
            >
              <BulletinAWMapStatic
                bulletin={bulletin}
                date={dateToISODateString(date)}
                region={bulletin.bulletinID}
                validTimePeriod={validTimePeriod}
              />
            </a>
          </Tooltip>
        </div>
        <ul className="list-plain list-bulletin-report-pictos">
            {regionNames.length > 1 && ( // NOTE: Requested by AINEVA
                <li>
                    <div
                        className="bulletin-report-regionNames"
                        style={{
                            // maxWidth: 380,
                            lineHeight: "1.25em",
                            paddingBottom: "0.25em",
                            borderRight: '0 none'
                        }}
                    >
                        {regionNames.join(", ")}
                    </div>
                </li>
            )}
          <li>
            <div className="bulletin-report-picto tooltip">
              <BulletinDangerRating dangerRatings={dangerRatings} />
            </div>
            {Array.isArray(bulletin.tendency) &&
              bulletin.tendency.map((tendency, index) => (
                <TendencyReport tendency={tendency} date={date} key={index} />
              ))}
          </li>
          {problems.map((p, index) => (
            <BulletinProblemItem key={index} problem={p} />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default BulletinDaytimeReport;

function TendencyReport({
  tendency,
  date
}: {
  tendency: Tendency;
  date: Date;
}) {
  const intl = useIntl();
  return (
    <Tooltip
      label={intl.formatMessage({
        id: "bulletin:report:tendency:hover"
      })}
    >
      <div className="bulletin-report-tendency">
        <span>
          <FormattedMessage
            id="bulletin:report:tendency"
            html={true}
            values={{
              strong: (...msg) => <strong className="heavy">{msg}</strong>,
              br: (...msg) => (
                <>
                  <br />
                  {msg}
                </>
              ),
              tendency: tendency.tendencyType
                ? intl.formatMessage({
                    id: `bulletin:report:tendency:${tendency.tendencyType}`
                  })
                : "–",
              daytime: "",
              date: intl.formatDate(
                tendency.validTime?.startTime
                  ? new Date(tendency.validTime?.startTime)
                  : getSuccDate(date),
                LONG_DATE_FORMAT
              )
            }}
          />
        </span>
        <TendencyIcon tendency={tendency.tendencyType} />
      </div>
    </Tooltip>
  );
}
