import React from "react";
import "leaflet/dist/leaflet.css";
import "./leaflet-player.css";
import { useParams } from "react-router-dom";
import LeafletMapControls from "./leaflet-map-controls";

import {
  MapContainer,
  TileLayer,
  AttributionControl,
  ScaleControl,
  MapContainerProps,
  TileLayerProps
} from "react-leaflet";
import { parseSearchParams } from "../../util/searchParams";

type Props = {
  loaded: boolean;
  gestureHandling: boolean;
  controls: boolean;
  mapConfigOverride: Partial<MapContainerProps>;
  tileLayerConfigOverride: Partial<TileLayerProps>;
  overlays: React.ReactElement;
  onInit: (map: L.Map) => void;
};

const LeafletMap = (props: Props) => {
  const params = useParams();
  const psp = parseSearchParams();
  const province = params?.province ?? psp.get("province");
  const province_bounds = config.map[`${province}.bounds`];
  const bounds = province_bounds ?? config.map.euregioBounds;
  // console.debug('d24883a8-e236-4ecb-a7e9-3e7e14995092', {province, psp, params, province_bounds, bounds})

  return (
    <MapContainer
      className={props.loaded ? "" : "map-disabled"}
      gestureHandling={props.gestureHandling}
      style={{
        width: "100%",
        height: "100%",
        zIndex: 1,
        opacity: 1
      }}
      zoomControl={false}
      {...{
        dragging: true,
        touchZoom: true,
        doubleClickZoom: true,
        scrollWheelZoom: true,
        boxZoom: true,
        keyboard: true,
        ...config.map.initOptions,
        ...props.mapConfigOverride
      }}
      bounds={bounds}
      attributionControl={false}
    >
      <AttributionControl prefix={config.map.attribution} />
      {props.loaded && <ScaleControl imperial={false} position="bottomleft" />}
      {props.loaded && props.controls}
      <TileLayer
        {...{
          ...config.map.tileLayer,
          ...props.tileLayerConfigOverride
        }}
      />
      {props.overlays}
      <LeafletMapControls {...props} />
    </MapContainer>
  );
};
export default LeafletMap;
