export interface ArchiveUrlQuery {
  date: string; // iso format like 2024-09-27
  // region?: string // like IT-21 or AINEVA or nothing
  lang?: string;
  bw?: boolean; // true if black/white, else color
}
export interface ArchiveUrlMap {
  pdf?: string;
  xml?: string; // some xml, like CAAMLv6 or the avalance.report version of CAAMLv6  // TODO: define?
  json?: string; // maybe a json representation of the avalance.report version of CAAMLv6
  map?: string; // thumbnail
}

export const getAinevaArchiveUrls: (
  query: ArchiveUrlQuery
) => ArchiveUrlMap = query => {
  // NOTE: "" was redirected to "IT_" if query.date <= "2022-05-06"
  if (query.date <= "2024-07") {
    return {
      pdf: window.config.template(config.apis.bulletin.pdf, {
        region: "IT_",
        date: query.date,
        lang: query.lang ?? "it",
        bw: query.bw ? "_bw" : ""
      }),
      xml: window.config.template(config.apis.bulletin.xml, {
        region: "",
        date: query.date,
        lang: query.lang ?? "it"
      }),
      json: window.config.template(config.apis.bulletin.json, {
        region: "",
        date: query.date,
        lang: query.lang ?? "it"
      })
    };
  }
  /* if (condition for future changes?) */ {
    return {
      pdf: window.config.template(config.apis.bulletin.pdf, {
        region: "AINEVA_",
        date: query.date,
        lang: query.lang ?? "it",
        bw: query.bw ? "_bw" : ""
      }),
      xml: window.config.template(config.apis.bulletin.xml, {
        region: "AINEVA_",
        date: query.date,
        lang: query.lang ?? "it"
      }),
      json: window.config.template(config.apis.bulletin.json, {
        region: "AINEVA_",
        date: query.date,
        lang: query.lang ?? "it"
      }),
      map: window.config.template(config.apis.bulletin.map, {
        date: query.date,
        publication: "",
        file: "fd_AINEVA_thumbnail",
        format: ".webp"
      })
    };
  }
};
