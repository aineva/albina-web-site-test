import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { useIntl } from "../../i18n";
import { dateToISODateString, LONG_DATE_FORMAT } from "../../util/date.js";
import ArchiveAwmapStatic from "../bulletin/bulletin-awmap-static";
import { Tooltip } from "../tooltips/tooltip";
import { type Bulletin, type Status } from "../../stores/bulletin";
import { RegionCodes } from "../../util/regions";
import { ArchiveUrlMap, getAinevaArchiveUrls } from "./archive-item-urls";

export type RegionBulletinStatus = {
  $type: "RegionBulletinStatus";
  status: Status;
  bulletin: Bulletin;
};
export type LegacyBulletinStatus = {
  $type: "LegacyBulletinStatus";
  status: Record<RegionCodes, string | undefined>;
};
export type BulletinStatus =
  | Status
  | RegionBulletinStatus
  | LegacyBulletinStatus;

type Props = {
  date: Date;
  status?: BulletinStatus;
};

function ArchiveItem({ date, status }: Props) {
  const intl = useIntl();
  const lang = intl.locale.slice(0, 2);
  const dateString = dateToISODateString(date);

  const urls = useMemo<ArchiveUrlMap>(
    () =>
      getAinevaArchiveUrls({
        // region
        lang,
        date: dateString
      }),
    [lang, dateString]
  );

  const [isAvailable, setIsAvailable] = useState<boolean | undefined>(
    undefined
  );
  useEffect(() => {
    let cancelled = false;
    (async () => {
      await new Promise(resolve => setTimeout(resolve, 100));
      if (cancelled) {
        return;
      }
      try {
        const res = await fetch(urls.pdf, {
          method: "head"
          // mode: 'no-cors',  // NOTE: opaque responses would hide 404 errors.
        });
        if (cancelled) {
          return;
        }
        if (res.ok) {
          setIsAvailable(true);
        } else {
          setIsAvailable(false);
        }
      } catch (e) {
        console.debug("Unable to head the archive PDF", e);
        setIsAvailable(false);
      }
    })();
    return () => {
      cancelled = true;
    };
  }, [urls]);

  if (isAvailable === false) {
    return null;
  }

  if (!isAvailable) {
    return (
      <tr>
        <td colSpan={3}>&nbsp;</td>
      </tr>
    );
  }

  return (
    <tr>
      <td>
        <strong>{intl.formatDate(date, LONG_DATE_FORMAT)}</strong>
      </td>
      <td>
        <ul className="list-inline list-buttongroup-dense list-download">
          <li>
            <DownloadLink href={urls.pdf}>PDF</DownloadLink>
          </li>
          <li>
            <DownloadLink href={urls.xml}>XML</DownloadLink>
          </li>
        </ul>
      </td>
      <td>
        {urls.map ? (
          <Link
            to={"/bulletin/" + dateString}
            className={"map-preview img tooltip"}
          >
            <img alt="" src={urls.map} />
          </Link>
        ) : null}
      </td>
    </tr>
  );
}

const DownloadLink: React.FC<
  React.PropsWithChildren<{
    href?: string;
  }>
> = props => {
  return (
    <a
      href={props.href}
      rel="noopener noreferrer"
      target="_blank"
      className="small secondary pure-button tooltip"
    >
      {props.children}
    </a>
  );
};

const BulletinMap: React.FC<{
  dateString: string;
  bulletin?: Bulletin;
}> = ({ bulletin, dateString }) => {
  const intl = useIntl();
  if (!showMap(dateString)) return <></>;
  const region =
    bulletin && dateString > "2022-05-06"
      ? `AINEVA_${bulletin.bulletinID}`
      : bulletin
      ? bulletin.bulletinID
      : dateString < "2022-05-06"
      ? "fd_albina_thumbnail"
      : "fd_AINEVA_thumbnail";
  return (
    <Tooltip
      label={intl.formatMessage({
        id: "archive:show-forecast:hover"
      })}
    >
      <Link
        to={"/bulletin/" + dateString}
        className={"map-preview img tooltip"}
      >
        <ArchiveAwmapStatic
          date={dateString}
          imgFormat=".jpg"
          region={region}
        />
      </Link>
    </Tooltip>
  );

  function showMap(dateString: string) {
    const lang = intl.locale.slice(0, 2);
    if (dateString < "2020-12-01") {
      switch (lang) {
        case "fr":
        case "es":
        case "ca":
        case "oc":
          return false;
        default:
          return true;
      }
    } else {
      return true;
    }
  }
};

export default ArchiveItem;
