import _p1 from "../../regions/micro-regions/IT-21_micro-regions.geojson.json";
import _p2 from "../../regions/micro-regions/IT-23_micro-regions.geojson.json";
import _p3 from "../../regions/micro-regions/IT-25_micro-regions.geojson.json";
import _p4 from "../../regions/micro-regions/IT-32-BZ_micro-regions.geojson.json";
import _p5 from "../../regions/micro-regions/IT-32-TN_micro-regions.geojson.json";
import _p6 from "../../regions/micro-regions/IT-34_micro-regions.geojson.json";
import _p7 from "../../regions/micro-regions/IT-36_micro-regions.geojson.json";
import _p8 from "../../regions/micro-regions/IT-57_micro-regions.geojson.json";

import _pe1 from "../../regions/micro-regions_elevation/IT-21_micro-regions_elevation.geojson.json";
import _pe2 from "../../regions/micro-regions_elevation/IT-23_micro-regions_elevation.geojson.json";
import _pe3 from "../../regions/micro-regions_elevation/IT-25_micro-regions_elevation.geojson.json";
import _pe4 from "../../regions/micro-regions_elevation/IT-32-BZ_micro-regions_elevation.geojson.json";
import _pe5 from "../../regions/micro-regions_elevation/IT-32-TN_micro-regions_elevation.geojson.json";
import _pe6 from "../../regions/micro-regions_elevation/IT-34_micro-regions_elevation.geojson.json";
import _pe7 from "../../regions/micro-regions_elevation/IT-36_micro-regions_elevation.geojson.json";
import _pe8 from "../../regions/micro-regions_elevation/IT-57_micro-regions_elevation.geojson.json";

import eawsRegions from "@eaws/outline_properties/index.json";
import type { Language } from "../appStore";
import { regionsRegex } from "../util/regions";

export interface MicroRegionProperties {
  id: string;
  start_date?: Date;
  end_date?: Date;
}

export const microRegions: MicroRegionProperties[] = [
  ..._p1.features, ..._p2.features, ..._p3.features, ..._p4.features, ..._p5.features, ..._p6.features, ..._p7.features, ..._p8.features
];

export interface MicroRegionElevationProperties {
  id: string;
  elevation: "high" | "low" | "low_high";
  "elevation line_visualization"?: number;
  threshold?: number;
  start_date?: Date;
  end_date?: Date;
}

export const microRegionsElevation: MicroRegionElevationProperties[] = [
  ..._pe1.features, ..._pe2.features, ..._pe3.features, ..._pe4.features, ..._pe5.features, ..._pe6.features, ..._pe7.features, ..._pe8.features
];

export interface RegionOutlineProperties {
  id: string;
  aws: {
    name: string;
    url: Record<Language, string>;
  }[];
}

/**
 * Determines whether the GeoJSON feature's start_date/end_date is valid today
 * @param {GeoJSON.Feature} feature the GeoJSON feature
 * @param {string} today the reference date
 * @returns {boolean}
 */
export function filterFeature(
  feature: GeoJSON.Feature,
  today = new Date().toISOString().slice(0, "2006-01-02".length)
): boolean {
  const properties = feature.properties;
  return (
    (!properties.start_date || properties.start_date <= today) &&
    (!properties.end_date || properties.end_date > today)
  );
}

export function eawsRegionIds(
  today = new Date().toISOString().slice(0, "2006-01-02".length)
): string[] {
  return eawsRegions
    .filter(properties => filterFeature({ properties }, today))
    .map(properties => properties.id)
    .filter(id => !regionsRegex.test(id));
}

export function microRegionIds(
  today = new Date().toISOString().slice(0, "2006-01-02".length)
): string[] {
  return microRegions
    //.filter(properties => filterFeature({ properties }, today))
    .map(f => String(f.id))
    .sort();
}
