import React, { useEffect, useState } from "react";
import { useIntl } from "../i18n";
import SmShare from "../components/organisms/sm-share.jsx";
import { getSuccDate } from "../util/date.js";
import { currentSeasonYear } from "../util/date-season";
import ArchiveItem from "../components/archive/archive-item.jsx";
import PageHeadline from "../components/organisms/page-headline.jsx";
import HTMLHeader from "../components/organisms/html-header";
import FilterBar from "../components/organisms/filter-bar.jsx";
import YearFilter from "../components/filters/year-filter.jsx";
import MonthFilter from "../components/filters/month-filter.jsx";
import { useSearchParams } from "react-router-dom";

function Archive() {
  const intl = useIntl();
  const lang = intl.locale.slice(0, 2);
  const [searchParams, setSearchParams] = useSearchParams();
  const [buttongroup] = useState(searchParams.get("buttongroup"));
  const minMonth = 9;
  const [month, setMonth] = useState(
    +(searchParams.get("month") || new Date().getMonth() + 1)
  );
  if (month < minMonth) setMonth(m => m + 12);
  const [year, setYear] = useState(
    +searchParams.get("year") || currentSeasonYear()
  );
  const [dates, setDates] = useState([] as Date[]);

  useEffect(() => {
    const dates = getDatesInMonth(year, month);
    setDates(dates);
    setSearchParams(
      buttongroup
        ? {
            year: String(year),
            month: String(month),
            buttongroup
          }
        : {
            year: String(year),
            month: String(month)
          },
      { replace: true }
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [month, year, setDates, setSearchParams, buttongroup, lang]);

  return (
    <>
      <HTMLHeader title={intl.formatMessage({ id: "more:archive:headline" })} />
      <PageHeadline
        title={intl.formatMessage({ id: "more:archive:headline" })}
        subtitle={intl.formatMessage({
          id: "more:subpages:subtitle"
        })}
      />
      <FilterBar search={false}>
        <YearFilter
          buttongroup={buttongroup}
          title={intl.formatMessage({
            id: "archive:filter:year"
          })}
          minYear={window.config.archive.minYear}
          maxYear={currentSeasonYear()}
          handleChange={setYear}
          formatter={y => `${y}/${y + 1}`}
          value={year}
        />
        {year && (
          <MonthFilter
            buttongroup={buttongroup}
            title={intl.formatMessage({
              id: "archive:filter:month"
            })}
            dateFormat={{ year: "2-digit", month: "short" }}
            handleChange={setMonth}
            length={10}
            minMonth={minMonth}
            value={month}
            year={year}
          />
        )}
      </FilterBar>
      <section className="section-padding-height">
        <section className="section-centered">
          <div className="table-container">
            <table className="pure-table pure-table-striped pure-table-small table-archive">
              <thead>
                <tr>
                  <th>
                    {intl.formatMessage({
                      id: "archive:table-header:date"
                    })}
                  </th>
                  <th>
                    {intl.formatMessage({
                      id: "archive:table-header:download"
                    })}
                  </th>
                  <th colSpan={99}></th>
                </tr>
              </thead>
              <tbody>
                {dates.map(d => (
                  <ArchiveItem key={d.getTime()} date={d} />
                ))}
              </tbody>
            </table>
          </div>
        </section>
      </section>
      <SmShare />
    </>
  );
}

export default Archive;

function getDatesInMonth(year: number, month: number): Date[] {
  const dates: Date[] = [];
  const startDate = new Date(year, month - 1, 1);
  for (
    let date = startDate;
    date.getMonth() === startDate.getMonth();
    date = getSuccDate(date)
  ) {
    dates.push(date);
  }
  return dates;
}
